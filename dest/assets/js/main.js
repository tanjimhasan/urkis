
/* ==============================================

    Template Name: Urkis
    -----------------------
    Template Author: AazzTech
    -------------------------
    Version: 1.0
    ------------


    <Table Of Content>


    01: Offcanvas Menu
    02: Check Data
    03: Owl Carousel
    04: Background Image
    05: Back To Top 

============================================== */

(function (t) {
    "use strict";
        let body = t('body');


        /*=================================
        01: Offcanvas Menu
        ==================================*/
        var windows = $(window);
        var screenSize = windows.width();

        $(".menu-trigger").on('click', function () {
            $(".offcanvas-menu").addClass('show');
            $(".shade").addClass('active');
            $(".main-header").addClass('offcanvasActive');
            $(".main-header").removeClass('sticky');
        });

        $(".offcanvas-close , .shade , .main-menu ul li a").on('click', function () {
            $(".offcanvas-menu").removeClass('show');
            $(".shade").removeClass('active');
            $(".main-header").removeClass('offcanvasActive');
            if( $(window).scrollTop() >= 100 ){
                $(".main-header").addClass("sticky");
            }
        });
        
        // Main Menu Apend To Offcanvas Menu
        if(screenSize<=991.98){
            $('.main-menu').appendTo('.offcanvas-menu .offcanvas-menu-inner');
        }

        $('.main-menu').find('ul li').parents('.main-menu ul li').addClass('has-sub-menu');
        $('.main-menu').find(".has-sub-menu").prepend('<span class="submenu-button"></span>'); 

        $('.offcanvas-menu-inner .main-menu').on("click", "li a , li.has-sub-menu , li .submenu-button" , function(e){
            
            if($(this).hasClass('has-submenu')){
                e.preventDefault();
                console.log($(this).siblings('ul'));
            }

            $(this).toggleClass("sub-menu-oppened");
            if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideUp();
            } else {
                $(this).siblings('ul').addClass('open').slideDown();
            }
             
        })

    
        /*==================================
        02: Check Data
        ====================================*/

        let checkData = function (data, value) {
            return typeof data === 'undefined' ? value : data;
        };


        /*==================================
        03: Owl Carousel
        ====================================*/

        let $owlCarousel = $('.owl-carousel');
            
        $owlCarousel.each(function () {
            let $t = $(this);
                
            $t.owlCarousel({
                items: checkData( $t.data('owl-items'), 1 ),
                margin: checkData( $t.data('owl-margin'), 0 ),
                loop: checkData( $t.data('owl-loop'), true ),
                smartSpeed: 450,
                autoplay: checkData( $t.data('owl-autoplay'), true ),
                autoplayTimeout: checkData( $t.data('owl-speed'), 8000 ),
                center: checkData( $t.data('owl-center'), false ),
                animateOut: checkData( $t.data('owl-animate'), false ),
                nav: checkData( $t.data('owl-nav'), false ),
                navText: ["<i class='fa fa-angle-right'></i>" , "<i class='fa fa-angle-left'></i>"],
                dots: checkData( $t.data('owl-dots'), false ),
                responsive: checkData( $t.data('owl-responsive'), {} )
            });
        });

        /* ===================================
        04: Background Image
        =====================================*/

        let bgImg = t('[data-bg-img]');

            bgImg.css('background', function(){
                return 'url(' + t(this).data('bg-img') + ') center center';
        });


        /* =================================
        05: Back To Top 
        ===================================*/

        let $backToTopBtn = $('.backToTop');

        if ($backToTopBtn.length) {
            let scrollTrigger = 300, // px
            backToTop = function () {
                let scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $backToTopBtn.addClass('show');
                } else {
                    $backToTopBtn.removeClass('show');
                }
            };

            backToTop();

            $(window).on('scroll', function () {
                backToTop();
            });

            $backToTopBtn.on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }

})(jQuery);